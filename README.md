# software-project2020

The idea for the software project is to implement different offscreen
techniques on a large touch-enabled display.




* Side the 'debug level' slider to see the various debug levels visually; the hospital curves are (top): force.alpha and (bottom) total x/y change squared: a measurement for the amount of movement of nodes in the graph. Note that the bottom curve is inverted, so that when both meet, you're getting close to done. :-)
* Click on node to expand or collapse. When a node has 'bundled' outgoing links, the first click will expand only those (a.k.a. 2nd display mode / expand state = 1), the next click will then expand the group node itself.
* Click on hull (which shows up when you expanded a group node) to collapse the group.
* Drag node to move entire graph around.

Notes
-----

- grid quantization is based on the biggest node, so that each node is guaranteed to fit in a single slot (this limits your viable positions, and hence the maximum number of nodes you'll be able to 'safely' fit in the given area, though!)

- 'tick' event does the magick: this attempt tried to cajole the force layout into playing nice by feeding it the quantized positions as .px/.py target data instead, riding the same ticket that the force-internal 'charge' code does. No dice. Influence is too little, and when you beef it up, you're getting toasted in all sorts of other ways.
